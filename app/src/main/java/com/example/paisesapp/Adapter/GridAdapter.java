package com.example.paisesapp.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.paisesapp.DetalleActivity;
import com.example.paisesapp.ListaActivity;
import com.example.paisesapp.Modelo.Pais;
import com.example.paisesapp.R;
import com.squareup.picasso.Picasso;

import java.util.List;

public class GridAdapter extends BaseAdapter {

    private Context context;
    private List<Pais> list;

    public GridAdapter(Context context, List<Pais> list) {
        this.context = context;
        this.list = list;
    }

    public Context getContext() {
        return context;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int i) {
        return list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        final int posicion = i;
        if(viewGroup != null){
            LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
            view = layoutInflater.inflate(R.layout.paises_item,null);

        }
        TextView tx = (TextView)  view.findViewById(R.id.txtNombre);
        ImageView  img=(ImageView) view.findViewById(R.id.imagen);
        tx.setText(list.get(i).getNombre());
        Picasso.with(getContext()).load(list.get(i).getUrl()).into(img);

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(), DetalleActivity.class);
                Pais pais =list.get(posicion);
                intent.putExtra("PAIS",pais);
                getContext().startActivity(intent);
            }
        });


        return view;

    }
}
