package com.example.paisesapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.VideoView;

import com.example.paisesapp.Modelo.Pais;
import com.squareup.picasso.Picasso;

public class DetalleActivity extends AppCompatActivity implements View.OnClickListener {

    TextView tx, tx1;
    ImageView img;
    WebView webView;
    String direccion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalle);

        img = (ImageView) findViewById(R.id.imagen);
        tx1 = findViewById(R.id.detallesPais);
        tx = (TextView) findViewById(R.id.textHimno);

        Bundle recibe = getIntent().getExtras();
        Pais p = (Pais) recibe.getSerializable("PAIS");
        getSupportActionBar().setTitle(p.getNombre());

        Picasso.with(this).load(p.getUrl()).into(img);
        webView = (WebView) findViewById(R.id.webPag);

        direccion = p.getHimno();

        webView.getSettings().setJavaScriptEnabled(true);
        webView.setWebChromeClient(new WebChromeClient() {
        });
        webView.loadData(p.getVideoUrl(), "text/html", "utf-8");


        if (p.getDetalles().length() > 0) {
            tx1.setText("Detalles: " + p.getDetalles());
        } else {
            tx1.setText("Detalles: No disponible");
        }


        tx.setText(direccion);
        tx.setOnClickListener(this);


    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.textHimno:
                irWeb(direccion);
                break;
            default:
                break;

        }
    }


    public void irWeb(String d) {
        Uri uri = Uri.parse(d);
        Intent intenNav = new Intent(Intent.ACTION_VIEW, uri);
        startActivity(intenNav);

    }

}
